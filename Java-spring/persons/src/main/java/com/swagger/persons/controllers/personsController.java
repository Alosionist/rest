package com.swagger.persons.controllers;

import com.swagger.persons.api.PersonsApi;
import com.swagger.persons.models.Person;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/persons")
public class personsController implements PersonsApi {

    private HashMap<String, Person> people;

    public personsController() {
        people = new HashMap<>();
        people.put("123456789", new Person().id("123456789").name("alon").age(19));
        people.put("348634544", new Person().id("348634544").name("aaa").age(15));
        people.put("763453448", new Person().id("763453448").name("bbb").age(25));
    }

    @Override
    @GetMapping("")
    public ResponseEntity<List<Person>> getAllPersons() {
        return new ResponseEntity<>(new ArrayList<>(people.values()), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Person> getPersonById(@PathVariable("id") String id) {
        return new ResponseEntity<>(people.get(id), HttpStatus.OK);
    }

    @Override
    @PostMapping
    public ResponseEntity<Void> addPerson(@RequestBody Person person) {
        people.put(person.getId(), person);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deletePerson(@PathVariable("id") String id) {
        people.remove(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
