import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Person } from '../person';
import { Observable } from 'rxjs';
import { PersonsService } from "../persons.service";

@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.css']
})
export class PersonComponent implements OnInit {
  @Input()
  public person: Person;

  @Output()
  close = new EventEmitter();
  
  constructor(private personsService: PersonsService) {}

  ngOnInit(): void {
  }

  deletePerson(): void {
    this.personsService.deletePerson(this.person.id)
      .subscribe(() => this.back());
  }

  back(): void {
    this.close.emit();
  }
}
