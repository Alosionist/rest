import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Coin, CryptoApiService } from '../crypto-api.service';

@Component({
  selector: 'app-crypto',
  templateUrl: './crypto.component.html',
  styleUrls: ['./crypto.component.css']
})
export class CryptoComponent implements OnInit {
  coins: any[];

  constructor(private cryptoApiService: CryptoApiService) { }

  ngOnInit(): void {
    this.cryptoApiService.getCoins().subscribe((res) => {
      this.coins = res.data.map(coin => {
        return {
          symbol: coin.symbol,
          value: coin.latest
        }
      });
      console.log(this.coins);
    })
  }
}
