import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common'; 
import { Person } from "../person";
import { Observable } from 'rxjs';
import { PersonsService } from "../persons.service";


@Component({
  selector: 'app-persons',
  templateUrl: './persons.component.html',
  styleUrls: ['./persons.component.css']
})
export class PersonsComponent implements OnInit {
  persons$: Observable<Person[]>;
  person: Person;

  constructor(private personsService: PersonsService) { }
              
  ngOnInit(): void {
    this.getPersons();
  }

  getPersons(): void {
    this.persons$ = this.personsService.getPersons();
  }

  select(person: Person): void {
    this.person = person;
  }
}
