import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PersonsComponent } from './persons/persons.component';
import { AddPersonComponent } from './add-person/add-person.component';
import { PersonComponent } from './person/person.component';
import { CryptoComponent } from './crypto/crypto.component';


const routes: Routes = [
  {path: 'persons', component: PersonsComponent},
  {path: 'add-person', component: AddPersonComponent},
  {path: 'crypto', component: CryptoComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
