import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Person } from "./person";
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PersonsService {
  readonly baseURL = '/api/persons';

  constructor(private http: HttpClient) { }

  public getPersons(): Observable<Person[]> {
    return this.http.get<Person[]>(this.baseURL);
  }

  public getPerson(id: string): Observable<Person> {
    return this.http.get<Person>(`${this.baseURL}/${id}`);
  }

  public deletePerson(id: string): Observable<any> {
    return this.http.delete(`${this.baseURL}/${id}`);
  }

  public addPerson(person: Person): Observable<any> {
    const headers = { 'content-type': 'application/json'}  
    const body = JSON.stringify(person);
    return this.http.post(this.baseURL, body,{'headers':headers});
  }
}
