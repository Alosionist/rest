import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface Coin {
  symbol: string;
  value: number;
}

@Injectable({
  providedIn: 'root'
})
export class CryptoApiService {
  readonly baseURL = '/coinbase/search?include_prices=true&limit=30&order=asc&page=1&resolution=day&sort=rank';

  constructor(private http: HttpClient) { }

  public getCoins(): Observable<any> {
    return this.http.get<any>(this.baseURL);
  }
}
